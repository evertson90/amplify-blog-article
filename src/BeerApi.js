import { API } from 'aws-amplify';

const API_NAME = "api83194554";
const PATH = "/beer";

export const postBeer = async (beer) => {
    return await API.post(API_NAME, PATH, {
        body: {
          name: beer.name,
          brand: beer.brand,
          type: beer.type
        }
      });
}

export const getBeer = async () => {
    return await API.get(API_NAME, PATH);
}

export const removeBeer = async (id) => {
    return await API.del(API_NAME, PATH + '/object/' + id);
}