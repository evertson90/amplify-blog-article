import React from 'react';
import { postBeer, getBeer, removeBeer } from './BeerApi';

export default class TableAndForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        name: "",
        brand: "",
        type: "",
        beer: []
      };
  
      this.handleInputChange = this.handleInputChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
      this.updateBeers();
    }

    updateBeers() {
      getBeer()
      .then((data) => {
        this.setState({
          beer: data
        })
      })
      .catch((e) => {
        console.log("getBeer failed");
      });
    }
  
    handleInputChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;
  
      this.setState({
        [name]: value
      });
    }

    handleSubmit(event) {
      console.log("New beer item post requested: " +  this.state.name);
      event.preventDefault();

      postBeer({
        name: this.state.name,
        brand: this.state.brand,
        type: this.state.type
      }).then((result) =>{
        this.updateBeers();
        this.setState({
          name: "",
          brand: "",
          type: ""
        })
      }).catch((e) => {
        console.log("postBeer failed");
      });

    }

    handleDelete(id) {
        console.log("Delete called for item with id: ", id);
        removeBeer(id)
          .then(() => {
            console.log("Beer removed");
            this.updateBeers();

          }).catch((e) => {
            console.log("Could not remove beer");
          });
    }
  
    render() {
      return (
        <div className="tableAndForm">
          
          <table className="beerTable">
              <tbody>
                <tr>
                    <th>Name</th>
                    <th>Brand</th>
                    <th>Type</th>
                    <th></th>
                </tr>
                {this.state.beer.map((item, key) =>
                    <tr key={item.id}>
                        <td>{item.name}</td>
                        <td>{item.brand}</td>
                        <td>{item.type}</td>
                        <td><button onClick={() => this.handleDelete(item.id) }>x</button></td>
                    </tr>
                )}
              </tbody>
          </table>
          
          <form className="beerForm" onSubmit={this.handleSubmit}>
            <label>name:</label>
            <input name="name" type="text" value={this.state.name} onChange={this.handleInputChange} />
            <br />
            <label>brand:</label>
            <input name="brand" type="text" value={this.state.brand} onChange={this.handleInputChange} />
            <br />
            <label> type:</label>
            <input name="type" type="text" value={this.state.type} onChange={this.handleInputChange} />
            <br />
            <input type="submit" value="Save" />
          </form>
        </div>
      );
    }
  }