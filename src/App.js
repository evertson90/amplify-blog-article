import React from 'react';
import './App.css';
import Amplify , {Auth} from 'aws-amplify';
import awsconfig from './aws-exports';
import { withAuthenticator } from 'aws-amplify-react';
import TableAndForm from './TableAndForm';

Amplify.configure(awsconfig);

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Beer</h1>        
        <button className="logout" onClick={() => Auth.signOut()}>Sign out</button>
      </header>
      <TableAndForm />
    </div>
  );
}

export default withAuthenticator(App, {usernameAttributes: 'email'});
